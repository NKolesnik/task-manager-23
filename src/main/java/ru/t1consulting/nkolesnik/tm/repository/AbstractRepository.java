package ru.t1consulting.nkolesnik.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.repository.IRepository;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    List<M> models = new ArrayList<>();

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        models.add(model);
        return model;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findById(id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll() {
        return models;
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        return models.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final Sort sort) {
        final Comparator<M> comparator = sort.getComparator();
        return findAll(comparator);
    }

    @Nullable
    @Override
    public M findByIndex(@NotNull final Integer index) {
        return models.get(index);
    }

    @Nullable
    @Override
    public M findById(@NotNull final String id) {
        return models.stream()
                .filter((i) -> id.equals(i.getId()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public M remove(@NotNull final M model) {
        models.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        @Nullable final M model = findById(id);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        @Nullable final M model = findByIndex(index);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collections) {
        models.removeAll(collections);
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public long getSize() {
        return models.size();
    }
}

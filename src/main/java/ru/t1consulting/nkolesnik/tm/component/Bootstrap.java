package ru.t1consulting.nkolesnik.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.repository.ICommandRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.IProjectRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.IUserRepository;
import ru.t1consulting.nkolesnik.tm.api.service.*;
import ru.t1consulting.nkolesnik.tm.command.AbstractCommand;
import ru.t1consulting.nkolesnik.tm.command.project.*;
import ru.t1consulting.nkolesnik.tm.command.system.*;
import ru.t1consulting.nkolesnik.tm.command.task.*;
import ru.t1consulting.nkolesnik.tm.command.user.*;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.exception.system.ArgumentNotSupportedException;
import ru.t1consulting.nkolesnik.tm.exception.system.CommandNotSupportedException;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.repository.CommandRepository;
import ru.t1consulting.nkolesnik.tm.repository.ProjectRepository;
import ru.t1consulting.nkolesnik.tm.repository.TaskRepository;
import ru.t1consulting.nkolesnik.tm.repository.UserRepository;
import ru.t1consulting.nkolesnik.tm.service.*;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new InfoCommand());
        registry(new AboutCommand());
        registry(new VersionCommand());
        registry(new HelpCommand());
        registry(new CommandsCommand());
        registry(new ArgumentsCommand());
        registry(new ExitCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByIProjectIdCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new UserRegistryCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserViewProfileCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveCommand());
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) {
            new ExitCommand().execute();
        }
        initUser();
        initData();
        initLogger();
        while (true) {
            try {
                System.out.println("Enter command:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **")));
    }

    private void initData() {
        @NotNull Project project = new Project();
        project.setName("Test proj");
        project.setDescription("Test descr");
        @NotNull final String userId = userService.findByLogin("test").getId();
        project.setUserId(userId);
        projectRepository.add(userId, project);
        @NotNull Project project2 = new Project();
        project2.setName("Admin proj");
        project2.setDescription("Admin descr");
        project2.setUserId(userService.findByLogin("admin").getId());
        projectRepository.add(project2.getUserId(), project2);
        @NotNull Task task = new Task();
        task.setName("Test task");
        task.setDescription("Test descr");
        task.setUserId(userService.findByLogin("test").getId());
        task.setProjectId(project.getId());
        taskRepository.add(task.getUserId(), task);
        @NotNull Task task2 = new Task();
        task2.setName("Admin task");
        task2.setDescription("Admin descr");
        task2.setUserId(userService.findByLogin("admin").getId());
        task2.setProjectId(project2.getId());
        taskRepository.add(task2.getUserId(), task2);
    }

    private void initUser() {
        userService.create("test", "test", "test@test.email.ru", Role.USUAL);
        userService.create("admin", "admin", "admin@admin.email.ru", Role.ADMIN);
    }

    private void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) {
            return false;
        }
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

}


